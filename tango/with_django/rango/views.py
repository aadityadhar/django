from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def index(request):
	context_dict = {'boldmessage' : "A working template"}
	return render(request,'rango/index.html', context = context_dict)

def about(request):
	context_dict1 = {'boldmessage' : "Created By Aaditya Dhar"}
	return render(request, 'rango/about.html', context = context_dict1)